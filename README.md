#  Example react-native-fbsdk with LoginManager, GraphRequest, GraphRequestManager (Facebook Graph API)

# General information
* [API Graph](https://developers.facebook.com/docs/graph-api/overview)
# Others links
* [Yours Facebook apps](https://developers.facebook.com/apps)
* [Facebook explorer](https://developers.facebook.com/tools/explorer/)
## :arrow_up: How to Setup

**Step 1:** git clone this repo:

**Step 2:** cd to the cloned repo:

**Step 3:** Install the Application with `yarn` or `npm i`


## :arrow_forward: How to Run App

1. cd to the repo
2. Run Build for either OS
  * for iOS
    * run `react-native run-ios`
  * for Android
    * Run Genymotion
    * run `react-native run-android`
