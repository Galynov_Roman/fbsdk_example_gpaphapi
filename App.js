/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
} from 'react-native';

import FBSDK from 'react-native-fbsdk';
const {
  LoginManager,
  GraphRequest,
  GraphRequestManager,
} = FBSDK;

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

const userPermissions = [
  'email',
  'public_profile',
  'user_birthday',
  'user_likes',
  'user_location',
  'user_photos',
  'user_posts',
  'user_status',
  'user_tagged_places',
  'user_videos',
  'pages_show_list',
];

type Props = {};
export default class App extends Component<Props> {

  onPressLogin = () => {
    LoginManager.logInWithReadPermissions(userPermissions).then((result) => {
        if (result.isCancelled) {
          alert('Login cancelled');
        } else {
          console.warn('Login success with permissions: '
            +result.grantedPermissions.toString());
          new GraphRequestManager().addRequest(this.infoRequest).start();
        }
      }, (error) => {
        alert('Login fail with error: ' + error);
      }
    );
  }

  infoRequest = new GraphRequest(
    '/me/feed', //?limit=1 if need added limit
    {
      parameters: {
        fields: {
          string: 'link, attachments'
        },
      },
    },
    this._responseInfoCallback,
  );

  _responseInfoCallback(error: ?Object, result: ?Object) {
    if (error) {
      console.warn('Error fetching data: ', error);
    } else {
      console.warn('Success fetching data: ', result);
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to React Native!
        </Text>
        <Text style={styles.instructions}>
          To get started, edit App.js
        </Text>
        <Text style={styles.instructions}>
          {instructions}
        </Text>
        <View>
        <Button
          onPress={() => this.onPressLogin()}
          title="get data from api"
          color="#3FB0FA"
        />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
